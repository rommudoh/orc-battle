# orc-battle

You face 12 opponents that are trying to kill you. Will you find a strategy to survive?

This game was inspired by the Orc Battle Game from the Book [Land of Lisp](https://nostarch.com/lisp.htm).

## Installation

No installation required. Simply run the program.

## Requirements

Running the program only requires a Java Runtime Environment (JRE). I would recommend OpenJDK 11.

## How to run

To run the program, execute `java -jar orc-battle-...-standalone.jar`.

## Building/Running from source

To build or run the program from source, you will need following:
- Clojure (I used version 1.10.2)
- Leiningen (I used version 2.9.6)

To build the jar, execute `lein uberjar`. The jar will be placed in the directory `target/uberjar`.

To run the program from source, execute `lein run`.

## License

Copyright © 2021 Julian Leyh <julian@vgai.de>

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
