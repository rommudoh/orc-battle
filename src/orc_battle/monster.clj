(ns orc-battle.monster)

(def builders (ref []))

;; Functions on a single monster
(defn create
  "create a monster with random health"
  []
  {:health (inc (rand-int 10))})

(defn dead?
  "check if a monster is dead"
  [{health :health :or {health 0}}]
  (<= health 0))

(defn kind
  "Returns a human-readable description of the Kind of Monster"
  [{kind :kind :or {kind :monster}}]
  (name kind))

(defmulti hit
  "applies the amount of damage to the monster,
   returning updated monster and hit message"
  (fn [monster _damage] (:kind monster)))
(defmethod hit :default [monster damage]
  (-> monster
      (update :health - damage)
      (#(if (dead? %)
          [% (str "You killed the "
                  (kind monster)
                  "!")]
          [% (str "You hit the "
                  (kind monster)
                  ", knocking off "
                  damage
                  " health points!")]))))

(defmulti describe
  "informative description of the monster"
  :kind)
(defmethod describe :default
  [monster]
  (str "A fierce " (kind monster)))

(defmulti attack
  "executes an attack"
  (fn [{:keys [monsters]} index] (:kind (get monsters index))))
(defmethod attack :default [gamestate _index] gamestate)

(defn attack-unless-dead
  "executes an attack unless the monster is dead"
  [{:keys [monsters] :as gamestate} index]
  (if (dead? (get monsters index))
    gamestate
    (attack gamestate index)))

;; Functions on a collection of monsters
(defn create-multiple
  "returns a vector of n randomly chosen monsters"
  [n]
  (into [] (repeatedly n #((get @builders (rand-int (count @builders)))))))

(defn all-dead?
  "check if all monsters are dead"
  [monsters]
  (every? dead? monsters))

(defn describe-all
  "returns a description of all monsters"
  [monsters]
  (->> monsters
       (map (fn [monster]
              (if (dead? monster)
                "**dead**"
                (str "(Health=" (:health monster) ") "
                     (describe monster)))))
       (map-indexed #(str "   " (inc %1) ". " %2 "\n"))
       (reduce str)
       (str "Your foes:\n")))

(defn rand-monster
  "returns a randomly chosen monster"
  [monsters]
  (let [index (rand-int (count monsters))
        monster (get monsters index)]
    (if (dead? monster)
      (recur monsters)
      [index monster])))

(defn pick
  "ask the user to pick a monster"
  [monsters]
  (println "Monster #:")
  (let [x (read)]
    (if (not (and (integer? x) (pos? x) (<= x (count monsters))))
      (do (println "That is not a valid monster number.")
          (recur monsters))
      (let [index (dec x)
            monster (get monsters index)]
        (if (dead? monster)
          (do (println "That monster is already dead.")
              (recur monsters))
          [index monster])))))

(defn do-moves
  "let the monsters do their move"
  ([gamestate] (do-moves gamestate 0))
  ([{:keys [monsters] :as gamestate} index]
   (if (> index (count monsters))
     gamestate
     (-> gamestate
         (attack-unless-dead index)
         (recur (inc index))))))

;; Orc
(defn create-orc []
  (-> (create)
      (assoc :kind :orc)
      (assoc :club-level (inc (rand-int 8)))))
(dosync (alter builders conj create-orc))
(defmethod describe :orc
  [{:keys [club-level]}]
  (str "A wicked orc with a level "
       club-level " club"))
(defmethod attack :orc
  [{:keys [monsters] :as gamestate} index]
  (let [monster (get monsters index)
        damage (inc (rand-int (:club-level monster)))]
    (println "An orc swings his club at you and knocks off"
             damage "of your health points.")
    (update-in gamestate [:player :health] - damage)))

;; Hydra
(defn create-hydra []
  (-> (create)
      (assoc :kind :hydra)))
(dosync (alter builders conj create-hydra))
(defmethod describe :hydra
  [{:keys [health]}]
  (str "A malicious hydra with "
       health " heads."))
(defmethod hit :hydra [monster damage]
  (-> monster
      (update :health - damage)
      (#(if (dead? %)
          [% "The corpse of the fully decapitated and decapacitated hydra falls to the floor!"]
          [% (str "You lop off "
                  damage
                  " of the hydra's heads!")]))))
(defmethod attack :hydra
  [{:keys [monsters] :as gamestate} index]
  (let [monster (get monsters index)
        damage (inc (rand-int (/ (:health monster) 2)))]
    (println "A hydra attacks you with"
             damage "of its heads! It also grows back one more head!")
    (-> gamestate
        (update-in [:player :health] - damage)
        (update-in [:monsters index :health] inc))))

;; Slime Mold
(defn create-slime-mold []
  (-> (create)
      (assoc :kind :slime-mold)
      (assoc :sliminess (inc (rand-int 5)))))
(dosync (alter builders conj create-slime-mold))
(defmethod describe :slime-mold
  [{:keys [sliminess]}]
  (str "A slime mold with a sliminess of " sliminess))
(defmethod attack :slime-mold
  [{:keys [monsters] :as gamestate} index]
  (let [monster (get monsters index)
        damage (inc (rand-int (:sliminess monster)))]
    (println "A slime mold wraps around your legs and decreases your agility by"
             damage "!")
    (-> gamestate
        (update-in [:player :agility] - damage)
        (update-in [:player :health]
                   #(if (zero? (rand-int 2))
                      (do (println "It also squirts in your face, taking away a health point!")
                          (dec %))
                      %)))))

;; Brigand
(defn create-brigand []
  (-> (create)
      (assoc :kind :brigand)))
(dosync (alter builders conj create-brigand))
(defmethod attack :brigand
  [{:keys [player] :as gamestate} _index]
  (let [{:keys [health agility strength]} player
        max-stat (max health agility strength)]
    (-> gamestate
        (update :player
                #(condp = max-stat
                   (:health %)
                   (do (println "A brigand hits you with his slingshot, taking off 2 health points!")
                       (update % :health - 2))
                   (:agility %)
                   (do (println "A brigand catches your leg with his whip, taking off 2 agility points!")
                       (update % :agility - 2))
                   (:strength %)
                   (do (println "A brigand cuts your arm with his whip, taking off 2 strength points!")
                       (update % :strength - 2)))))))

