(ns orc-battle.core
  (:require [orc-battle.player :as p]
            [orc-battle.monster :as m])
  (:gen-class))

(defn init [monster-count]
  {:player (p/init)
   :monsters (m/create-multiple monster-count)})

(defn game-loop
  "runs the game loop"
  [{:keys [player monsters] :as gamestate}]
  (if (or (p/dead? player) (m/all-dead? monsters))
    gamestate
    (do (println (p/describe player))
        (-> gamestate
            (p/do-moves)
            (m/do-moves)
            (recur)))))

(defn -main
  "runs the game"
  [& _args]
  (-> (init 12)
      (game-loop)
      (#(cond
          (p/dead? (:player %))
          (println "You have been killed. Game Over.")
          (m/all-dead? (:monsters %))
          (println "Congratulations! You have vanquished all of your foes.")))))
