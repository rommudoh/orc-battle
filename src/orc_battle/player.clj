(ns orc-battle.player
  (:require [orc-battle.monster :as m]))

(defn init [] {:health 30
               :agility 30
               :strength 30})

(defn dead? [{health :health :or {health 0}}]
  (<= health 0))

(defn describe [{:keys [health agility strength]}]
  (str "You are a valiant knight with a health of "
       health ", an agility of " agility
       ", and a strength of " strength "."))

;; Functions that need whole gamestate
(defn do-roundhouse
  "execute multiple attacks against randomly chosen monsters"
  ([{:keys [player] :as gamestate}]
   (do-roundhouse gamestate (inc (rand-int (quot (:strength player) 3)))))
  ([{:keys [monsters] :as gamestate} count]
   (if (or (<= count 0) (m/all-dead? monsters))
     gamestate
     (let [[index old-monster] (m/rand-monster monsters)
           [new-monster hit-message] (m/hit old-monster 1)
           new-gamestate (update gamestate :monsters
                                 #(assoc-in % [index] new-monster))]
       (println hit-message)
       (recur new-gamestate (dec count))))))

(defn do-attack
  "let player choose their attack, return new gamestate"
  [{:keys [player monsters] :as gamestate}]
  (println "Attack style: [s]tab [d]ouble swing [r]oundhouse:")
  (case (read)
    s (let [damage (+ 2 (inc (rand-int (quot (:strength player) 2))))
             [index old-monster] (m/pick monsters)
             [new-monster hit-message] (m/hit old-monster damage)]
         (println hit-message)
         (update gamestate :monsters
                 #(assoc-in % [index] new-monster)))
    d (let [damage (inc (rand-int (quot (:strength player) 6)))]
         (println "Your double swing has a strength of" damage)
         (let [[index old-monster] (m/pick monsters)
               [new-monster hit-message] (m/hit old-monster damage)
               new-monsters (assoc-in monsters [index] new-monster)
               new-gamestate (assoc gamestate :monsters new-monsters)]
           (println hit-message)
           (if (m/all-dead? new-monsters)
             new-gamestate
             (let [[index old-monster] (m/pick new-monsters)
                   [new-monster hit-message] (m/hit old-monster damage)]
               (println hit-message)
               (update new-gamestate :monsters
                       #(assoc-in % [index] new-monster))))))
    (do-roundhouse gamestate)))

(defn do-moves
  "let player do their move"
  ([{:keys [player] :as gamestate}]
   (do-moves gamestate (inc (quot (max 0 (:agility player)) 15))))
  ([{:keys [monsters] :as gamestate} moves]
   (if (or (<= moves 0) (m/all-dead? monsters))
     gamestate
     (do (println (m/describe-all monsters))
         (-> gamestate
             (do-attack)
             (recur (dec moves)))))))
