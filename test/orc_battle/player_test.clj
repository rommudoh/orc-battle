(ns orc-battle.player-test
  (:require [clojure.test :refer [deftest is]]
            [orc-battle.player :as p]))

(def initialized-player (p/init))
(def dead-player (assoc initialized-player :health 0))
(def very-dead-player (assoc initialized-player :health -30))

(deftest test-player-initialization
  (let [{:keys [health agility strength]} initialized-player]
    (is (not (nil? initialized-player)))
    (is (= health 30))
    (is (= agility 30))
    (is (= strength 30)))
  (let [{:keys [health]} dead-player]
    (is (= health 0)))
  (let [{:keys [health]} very-dead-player]
    (is (= health -30))))

(deftest test-player-dead?
  (is (not (p/dead? initialized-player)))
  (is (p/dead? dead-player))
  (is (p/dead? very-dead-player)))

(deftest test-player-description
  (is (= (p/describe initialized-player)
         "You are a valiant knight with a health of 30, an agility of 30, and a strength of 30."))
  (is (= (p/describe dead-player)
         "You are a valiant knight with a health of 0, an agility of 30, and a strength of 30."))
  (is (= (p/describe very-dead-player)
         "You are a valiant knight with a health of -30, an agility of 30, and a strength of 30.")))
