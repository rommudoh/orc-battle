(ns orc-battle.monster-test
  (:require [clojure.test :refer [deftest is]]
            [orc-battle.monster :as m]))

(def live-monster (m/create))
(def dead-monster (assoc live-monster :health 0))
(def very-dead-monster (assoc live-monster :health -30))

(deftest test-create
  (is (> (:health (m/create)) 0)))

(deftest test-dead-monster
  (is (not (m/dead? live-monster)))
  (is (m/dead? dead-monster))
  (is (m/dead? very-dead-monster)))

(deftest test-orc
  (let [orc (m/create-orc)]
    (is (= (:kind orc) :orc))
    (is (> (:club-level orc) 0))
    (is (= (m/kind orc) "orc"))
    (is (= (m/describe orc)
           (str "A wicked orc with a level "
                (:club-level orc) " club")))))

(deftest test-hydra
  (let [hydra (m/create-hydra)]
    (is (= (:kind hydra) :hydra))
    (is (= (m/kind hydra) "hydra"))
    (is (= (m/describe hydra)
           (str "A malicious hydra with "
                (:health hydra) " heads.")))))

(deftest test-create-multiple
  (is (= (count (m/create-multiple 12)) 12)))

(deftest test-all-dead
  (is (not (m/all-dead? (m/create-multiple 3))))
  (is (m/all-dead? [dead-monster very-dead-monster])))

(deftest test-describe-all
  (let [orc (m/create-orc)
        hydra (m/create-hydra)
        monster (m/create)
        monsters [orc hydra monster]]
    (is (= (m/describe-all monsters)
           (str "Your foes:\n"
                "   1. (Health="
                (:health orc)
                ") A wicked orc with a level "
                (:club-level orc)
                " club\n"
                "   2. (Health="
                (:health hydra)
                ") A malicious hydra with "
                (:health hydra)
                " heads.\n"
                "   3. (Health="
                (:health monster)
                ") A fierce "
                (m/kind monster)
                "\n")))))
