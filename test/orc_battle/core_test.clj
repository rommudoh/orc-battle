(ns orc-battle.core-test
  (:require [clojure.test :refer [deftest is]]
            [orc-battle.core :as c]))

(def monster-count 12)
(def initialized-game (c/init monster-count))

(deftest test-player-initialization
  (is (not (nil? (:player initialized-game)))))

(deftest test-monster-initialization
  (is (not (nil? (:monsters initialized-game))))
  (is (= (count (:monsters initialized-game)) monster-count)))
